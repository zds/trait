package fi.iki.zds.trait;

public class AttributeDefinition {
    private final String name;
    private final String shortName;

    public AttributeDefinition(String name, String shortName) {
        this.name = name;
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }
}
