package fi.iki.zds.trait;

import java.util.SortedMap;
import java.util.TreeMap;

public class PraedorConstants {
    public static final SortedMap<String, AttributeDefinition> ATTRIBUTE_DEFINITIONS;
    
    static {
        AttributeDefinition[] ATTRIBUTE_DEFINITION_ARRAY = {
            new AttributeDefinition("Voima", "VMA"),
            new AttributeDefinition("Terveys", "TRV"),
            new AttributeDefinition("Ketteryys", "KTR"),
            new AttributeDefinition("Valppaus", "VLP"),
            new AttributeDefinition("Sisukkuus", "SIS"),
            new AttributeDefinition("Karisma", "KAR")
        };
        
        SortedMap<String, AttributeDefinition> attributeDefinitions =
                new TreeMap<String, AttributeDefinition>();
        for (AttributeDefinition attributeDefinition : ATTRIBUTE_DEFINITION_ARRAY) {
            attributeDefinitions.put(attributeDefinition.getShortName(),
                    attributeDefinition);
        }
        ATTRIBUTE_DEFINITIONS = attributeDefinitions;
    }
}
