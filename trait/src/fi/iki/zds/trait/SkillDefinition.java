package fi.iki.zds.trait;

import java.util.Map;

public abstract class SkillDefinition {
    public abstract int getBaseValue(Map<AttributeDefinition, Attribute> attributes);
}
