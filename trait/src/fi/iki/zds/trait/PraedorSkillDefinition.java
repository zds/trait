package fi.iki.zds.trait;

import java.util.Map;

public class PraedorSkillDefinition extends SkillDefinition {
    private final String name;
    private final AttributeDefinition baseAttribute;
    private final Integer baseValue;
    
    public PraedorSkillDefinition(String name, AttributeDefinition baseAttribute) {
        this.name = name;
        this.baseValue = null;
        this.baseAttribute = baseAttribute;
    }
    
    public PraedorSkillDefinition(String name, int baseValue) {
        this.name = name;
        this.baseValue = baseValue;
        this.baseAttribute = null;
    }

    @Override
    public int getBaseValue(Map<AttributeDefinition, Attribute> attributes) {
        if (baseAttribute == null) {
            return baseValue;
        }
        
        Attribute attribute = attributes.get(baseAttribute);
        
        return (attribute.getValue()+1) >> 1;
    }

    public String getName() {
        return name;
    }
}
